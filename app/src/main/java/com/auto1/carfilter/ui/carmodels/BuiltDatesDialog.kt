package com.auto1.carfilter.ui.carmodels

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.Nullable
import androidx.appcompat.app.AppCompatDialogFragment
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.auto1.carfilter.R
import com.auto1.carfilter.model.Car
import com.auto1.carfilter.utils.gone
import com.auto1.carfilter.utils.visible


/**
 * Created by Rax on 2019-04-28.
 */
class BuiltDatesDialog : AppCompatDialogFragment() {
    var rvDates: RecyclerView? = null
    var summary: String? = ""
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.dialog_built_dates, container, false)
        rvDates = v?.findViewById(R.id.manufacturer_list)
        val tvSummary: TextView = v?.findViewById(R.id.tv_summary)!!
        rvDates?.layoutManager = LinearLayoutManager(activity)
        val adapter = CarItemsAdapter {
            rvDates?.gone()
            summary += "\nYear:" + it.carName
            tvSummary.visible()
            tvSummary.text = summary
            dialog.setTitle(activity?.getString(R.string.lbl_sumary))

        }
        rvDates!!.adapter = adapter
        arguments?.let {
            val name = arguments?.getString(CompanyName)
            val model = arguments?.getString(ModelName)
            dialog.setTitle(model)
            var car: Car = arguments?.getSerializable(BuiltDate) as Car
            summary = "Manufacturer:$name\nModel:$model"
            adapter.addItems(car.listBuiltDate!!)
        }
        return v
    }

    override fun onCreate(@Nullable savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NORMAL, R.style.Theme_AppCompat_Light_Dialog_Alert)
    }

    companion object {
        private const val CompanyName: String = "company_name"
        private const val ModelName: String = "model_name"
        private const val BuiltDate: String = "built_date"
        fun newInstance(company: String?, model: String?, list: Car): BuiltDatesDialog {
            return BuiltDatesDialog().apply {
                arguments = Bundle().apply {
                    putString(CompanyName, company)
                    putString(ModelName, model)
                    putSerializable(BuiltDate, list)
                }
            }
        }
    }
}