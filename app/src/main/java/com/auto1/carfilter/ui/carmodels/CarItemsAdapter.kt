package com.auto1.carfilter.ui.carmodels

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.auto1.carfilter.R
import com.auto1.carfilter.model.Car
import com.auto1.carfilter.utils.inflate
import kotlinx.android.synthetic.main.row_car_filter.view.*

/**
 * Created by Rax on 2019-04-27.
 */

class CarItemsAdapter(
    private val listener: (Car) -> Unit
) : RecyclerView.Adapter<CarItemsAdapter.ItemHolder>() {

    /**
     * List of manufacturer items
     */
    private var manufacturer: MutableList<Car> = arrayListOf()

    /**
     * Inflate the view
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ItemHolder(parent.inflate(R.layout.row_car_filter))

    /**
     * Bind the view with the data
     */
    override fun onBindViewHolder(itemHolder: ItemHolder, position: Int) =
        itemHolder.bind(manufacturer[position], listener)

    /**
     * Number of items in the list to display
     */
    override fun getItemCount() = manufacturer.size

    /**
     * View Holder Pattern
     */
    class ItemHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        /**
         * Binds the UI with the data and handles clicks
         */
        fun bind(manufacturer: Car, listener: (Car) -> Unit) = with(itemView) {
            tv_tittle.text = manufacturer.carName
            setOnClickListener { listener(manufacturer) }
        }

    }

    /**
     * Swap function to set new data on updating
     */
    fun addItems(items: List<Car>) {
        manufacturer.addAll(items)
        notifyDataSetChanged()
    }
}