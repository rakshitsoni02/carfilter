package com.auto1.carfilter.ui.manufacturer

import androidx.lifecycle.*
import com.auto1.carfilter.AppExecutors
import com.auto1.carfilter.model.Manufacturer
import com.auto1.carfilter.model.network.Resource
import com.auto1.carfilter.repo.FilterRepository
import javax.inject.Inject

/**
 * A container for [Manufacturer] related data to show on the UI.
 */
class ManufacturerViewModel @Inject constructor(
    private val filterRepository: FilterRepository,
    private val appExecutors: AppExecutors
) : ViewModel() {
    private val page = MutableLiveData<Int>()
    val results: LiveData<Resource<List<Manufacturer>>> = Transformations
        .switchMap(page) { search ->
            filterRepository.getManufacturers(search)


        }

    /**
     * Return manufacturers to observe on the UI.
     */
    fun loadInitialPage() {
        page.value = 0
    }

    private var loading: Boolean = false
    fun loadNextPage() {
        page.value?.let {
            appExecutors.networkIO().execute {
                var count: Int = filterRepository.getMaxPageCountManufacturer()
                if (page.value!! + 1 < count) {
                    appExecutors.mainThread().execute {
                        page.value = page.value!! + 1
                    }
                }
            }
        }

    }

}