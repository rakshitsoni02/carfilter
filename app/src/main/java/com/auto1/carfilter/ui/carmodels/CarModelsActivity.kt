package com.auto1.carfilter.ui.carmodels

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.auto1.carfilter.R
import com.auto1.carfilter.model.Car
import com.auto1.carfilter.ui.base.BaseActivity
import com.auto1.carfilter.utils.getViewModel
import com.auto1.carfilter.utils.load
import com.auto1.carfilter.utils.observe
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.empty_layout.*
import kotlinx.android.synthetic.main.progress_layout.*


class CarModelsActivity : BaseActivity() {
    private val carModelViewModel by lazy { getViewModel<CarModelViewModel>() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.acticity_cars)
        manufacturer_list.setEmptyView(empty_view)
        manufacturer_list.setProgressView(progress_view)
        initRecyclerView()
    }

    var adapter: CarItemsAdapter? = null
    private fun initRecyclerView() {
        intent.extras?.let {
            carModelViewModel.companyId = intent.extras?.getString(CompanyId)!!
            supportActionBar?.title = intent.extras?.getString(CompanyName)!!
        }

        adapter = CarItemsAdapter {
            carModelViewModel.carName = it.carName
            carModelViewModel.loadBuiltDates(it.carId)
        }
        manufacturer_list.adapter = adapter
        manufacturer_list.layoutManager = LinearLayoutManager(this)
        observe()
        carModelViewModel.loadInitialPage()
        setScrollListener()
    }

    private fun setScrollListener() {
        manufacturer_list.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                val layoutManager = recyclerView.layoutManager as LinearLayoutManager
                val lastPosition = layoutManager.findLastVisibleItemPosition()
                if (lastPosition == adapter?.itemCount!! - 1
                    && progress_view.visibility != View.VISIBLE
                ) {
                    carModelViewModel.loadNextPage()
                }
            }
        })
    }

    private fun observe() {
        carModelViewModel.results.observe(this) {
            it.load(manufacturer_list) {
                it?.let {
                    adapter?.addItems(it)
                }
            }
        }
        carModelViewModel.builtYears.observe(this) {
            it.load {
                it?.let {
                    var car = Car()
                    car.listBuiltDate = it
                    val fragment =
                        BuiltDatesDialog.newInstance(
                            supportActionBar?.title.toString(),
                            carModelViewModel.carName, car
                        )
                    fragment.show(supportFragmentManager, "some tag")
                }
            }
        }
    }

    companion object {
        private const val CompanyId: String = "CompanyId"
        private const val CompanyName: String = "CompanyName"
        fun getStartIntent(ctx: Context, manufacturerId: String, mName: String?): Intent {
            val intent = Intent(ctx, CarModelsActivity::class.java)
            val b = Bundle()
            b.putString(CompanyId, manufacturerId)
            b.putString(CompanyName, mName)
            intent.putExtras(b)
            return intent
        }
    }
}
