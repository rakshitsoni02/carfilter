package com.auto1.carfilter.ui.carmodels

import androidx.lifecycle.*
import com.auto1.carfilter.AppExecutors
import com.auto1.carfilter.model.Car
import com.auto1.carfilter.model.Manufacturer
import com.auto1.carfilter.model.network.Resource
import com.auto1.carfilter.repo.FilterRepository
import javax.inject.Inject

/**
 * A container for [Cars] related data to show on the UI.
 */
class CarModelViewModel @Inject constructor(
    private val filterRepository: FilterRepository,
    private val appExecutors: AppExecutors
) : ViewModel() {
    private val page = MutableLiveData<Int>()
    private val carId = MutableLiveData<String>()
    var companyId: String = ""
    var carName: String? = ""

    val results: LiveData<Resource<List<Car>>> = Transformations
        .switchMap(page) { search ->
            filterRepository.getCarsForCompany(search, companyId)
        }

    val builtYears: LiveData<Resource<List<Car>>> = Transformations
        .switchMap(carId) { search ->
            filterRepository.getBuiltDates(companyId,search)
        }

    /**
     * Return manufacturers to observe on the UI.
     */
    fun loadInitialPage() {
        page.value = 0
    }

    fun loadBuiltDates(id: String) {
        carId.value = id
    }

    fun loadNextPage() {
        page.value?.let {
            appExecutors.networkIO().execute {
                var count: Int = filterRepository.getMaxPageCountCar(companyId)
                if (page.value!! + 1 < count) {
                    appExecutors.mainThread().execute {
                        page.value = page.value!! + 1
                    }
                }
            }
        }

    }

}