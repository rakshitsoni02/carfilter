package com.auto1.carfilter

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.auto1.carfilter.ui.base.BaseActivity
import com.auto1.carfilter.ui.carmodels.CarModelsActivity
import com.auto1.carfilter.ui.common.FilterItemsAdapter
import com.auto1.carfilter.ui.manufacturer.ManufacturerViewModel
import com.auto1.carfilter.utils.getViewModel
import com.auto1.carfilter.utils.load
import com.auto1.carfilter.utils.observe
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.empty_layout.*
import kotlinx.android.synthetic.main.progress_layout.*

class MainActivity : BaseActivity() {
    private val manufacturerViewModel by lazy { getViewModel<ManufacturerViewModel>() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        // Setting up RecyclerView and adapter
        manufacturer_list.setEmptyView(empty_view)
        manufacturer_list.setProgressView(progress_view)
        initRecyclerView()
    }

    var adapter: FilterItemsAdapter? = null
    private fun initRecyclerView() {
        adapter = FilterItemsAdapter {
            startActivity(
                CarModelsActivity.getStartIntent(
                    this
                    , it.manufacturerId,
                    it.manufacturerName
                )
            )
        }
        manufacturer_list.adapter = adapter
        manufacturer_list.layoutManager = LinearLayoutManager(this)
        observe()
        manufacturerViewModel.loadInitialPage()
        setScrollListener()
    }

    private fun setScrollListener() {
        manufacturer_list.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                val layoutManager = recyclerView.layoutManager as LinearLayoutManager
                val lastPosition = layoutManager.findLastVisibleItemPosition()
                if (lastPosition == adapter?.itemCount!! - 1 && progress_view.visibility != View.VISIBLE) {
                    manufacturerViewModel.loadNextPage()
                }
            }
        })
    }

    private fun observe() {
        manufacturerViewModel.results.observe(this) {
            it.load(manufacturer_list) {
                it?.let {
                    adapter?.addItems(it)
                }
            }
        }
    }
}
