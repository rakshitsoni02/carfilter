package com.auto1.carfilter.repo

import androidx.lifecycle.LiveData
import androidx.lifecycle.LiveDataReactiveStreams
import com.auto1.carfilter.AppExecutors
import com.auto1.carfilter.BuildConfig
import com.auto1.carfilter.api.CarFilterService
import com.auto1.carfilter.db.CarFilterDao
import com.auto1.carfilter.model.Car
import com.auto1.carfilter.model.CarDataResponse
import com.auto1.carfilter.model.Manufacturer
import com.auto1.carfilter.model.network.Resource
import io.reactivex.Flowable
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class FilterRepository @Inject constructor(
    private val carFilterDao: CarFilterDao,
    private val carFilterService: CarFilterService,
    private val appExecutors: AppExecutors = AppExecutors()
) {

    /**
     * Fetch the Manufacturers from database if exist else fetch from web
     * and persist them in the database
     */
    fun getManufacturers(page: Int): LiveData<Resource<List<Manufacturer>>> {
        return object : NetworkBoundResource<List<Manufacturer>, CarDataResponse>(appExecutors) {
            override fun saveCallResult(item: CarDataResponse) {
                var list: MutableList<Manufacturer> = arrayListOf()
                val manufacturers = item.data.asJsonObject
                for (entry in manufacturers.entrySet()) {
                    list.add(
                        Manufacturer(
                            entry.value.toString().replace("\"", ""),
                            entry.key.toString().replace("\"", ""),
                            item.page, item.totalPage
                        )
                    )
                }
                carFilterDao.insertCarManufacturers(list)
            }

            override fun shouldFetch(data: List<Manufacturer>?) = (data == null || data.isEmpty())

            override fun loadFromDb() = carFilterDao.getManufacturers(page.toString())

            override fun createCall() =
                carFilterService.getManufacturers(
                    page.toString(), "15",
                    BuildConfig.FILTER_API_KEY
                )
        }.asLiveData()
    }

    /**
     * Fetch the Cars from database if exist else fetch from web
     * and persist them in the database
     */
    fun getCarsForCompany(page: Int, companyId: String): LiveData<Resource<List<Car>>> {
        return object : NetworkBoundResource<List<Car>, CarDataResponse>(appExecutors) {
            override fun saveCallResult(item: CarDataResponse) {
                var list: MutableList<Car> = arrayListOf()
                val manufacturers = item.data.asJsonObject
                for (entry in manufacturers.entrySet()) {
                    list.add(
                        Car(
                            entry.value.toString().replace("\"", ""),
                            entry.key.toString().replace("\"", ""),
                            companyId,
                            item.page, item.totalPage
                        )
                    )
                }
                carFilterDao.insertCarModels(list)
            }

            override fun shouldFetch(data: List<Car>?) = (data == null || data.isEmpty())

            override fun loadFromDb() = carFilterDao.getCars(page.toString(), companyId)

            override fun createCall() =
                carFilterService.getCarModels(
                    companyId,
                    page.toString(), "15", BuildConfig.FILTER_API_KEY
                )
        }.asLiveData()
    }

    fun getBuiltDates(companyId: String, carId: String) = object : NetworkBoundResourceNoRoom<List<Car>,
            CarDataResponse>(appExecutors) {
        override fun transformToResult(item: CarDataResponse): LiveData<List<Car>> {
            var list: MutableList<Car> = arrayListOf()
            val manufacturers = item.data.asJsonObject
            for (entry in manufacturers.entrySet()) {
                list.add(
                    Car(
                        entry.value.toString().replace("\"", ""),
                        entry.key.toString().replace("\"", ""),
                        companyId,
                        0
                    )
                )
            }
            return transformToListData(list)
        }

        override fun createCall() = carFilterService.getCarBuiltDates(
            companyId,
            carId, BuildConfig.FILTER_API_KEY
        )

    }.asLiveData()

    fun transformToListData(list: List<Car>): LiveData<List<Car>> {
        val p = Flowable.fromArray(list)
        return LiveDataReactiveStreams.fromPublisher(p)
    }

    /**
     * Fetch MaxPageCount for Manufacturer
     */
    fun getMaxPageCountManufacturer(): Int {
        return carFilterDao.loadByMaxCountManufacturer()?.totalPage!!
    }

    /**
     * Fetch MaxPageCount for Cars
     */
    fun getMaxPageCountCar(companyId: String): Int {
        return carFilterDao.loadByMaxCountForCar(companyId)?.totalPage!!
    }

}