import com.auto1.carfilter.model.network.Resource
import retrofit2.Response
import retrofit2.Retrofit

/**
 * Created by Rax on 2019-04-26.
 */
/**
 * Synthetic sugaring to create Retrofit Service.
 */
inline fun <reified T> Retrofit.create(): T = create(T::class.java)

