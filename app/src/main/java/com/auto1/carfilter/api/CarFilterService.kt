package com.auto1.carfilter.api

import androidx.lifecycle.LiveData
import com.auto1.carfilter.BuildConfig
import com.auto1.carfilter.model.CarDataResponse
import com.auto1.carfilter.model.network.Resource
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * REST API access points
 */
interface CarFilterService {


    @GET("car-types/manufacturer")
    fun getManufacturers(
        @Query("page") pageNo: String,
        @Query("pageSize") pageSize: String,
        @Query("wa_key") key: String
    ): LiveData<ApiResponse<CarDataResponse>>

    @GET("car-types/main-types")
    fun getCarModels(
        @Query("manufacturer") companyId: String,
        @Query("page") pageNo: String,
        @Query("pageSize") pageSize: String,
        @Query("wa_key") key: String
    ): LiveData<ApiResponse<CarDataResponse>>

    @GET("car-types/built-dates")
    fun getCarBuiltDates(
        @Query("manufacturer") companyId: String,
        @Query("main-type") carId: String,
        @Query("wa_key") key: String
    ): LiveData<ApiResponse<CarDataResponse>>
}
