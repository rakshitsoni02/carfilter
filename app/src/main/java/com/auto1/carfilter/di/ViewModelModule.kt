package com.auto1.carfilter.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.auto1.carfilter.di.base.ViewModelFactory
import com.auto1.carfilter.di.base.ViewModelKey
import com.auto1.carfilter.ui.carmodels.CarModelViewModel
import com.auto1.carfilter.ui.manufacturer.ManufacturerViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(ManufacturerViewModel::class)
    abstract fun bindManufacturerViewModel(manufacturerViewModel: ManufacturerViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CarModelViewModel::class)
    abstract fun bindCarModelViewModel(carModelViewModel: CarModelViewModel): ViewModel

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory
}
