package com.auto1.carfilter.di

import android.app.Application
import androidx.room.Room
import com.auto1.carfilter.db.FilterDatabase
import com.auto1.carfilter.api.CarFilterService
import com.auto1.carfilter.db.CarFilterDao
import com.auto1.carfilter.utils.LiveDataCallAdapterFactory
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton


@Module
class AppModule {

    @Singleton
    @Provides
    fun provideCarFilterService(): CarFilterService {
//      val client=  OkHttpClient.Builder()
//            .addNetworkInterceptor(StethoInterceptor())
//            .build()

        return Retrofit.Builder()
            .baseUrl(BASE_URL)
//            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(LiveDataCallAdapterFactory())
            .build()
            .create(CarFilterService::class.java)
    }

    @Singleton
    @Provides
    fun provideDb(app: Application): FilterDatabase {
        return Room.databaseBuilder(app, FilterDatabase::class.java, "filter-db").build()
    }

    @Singleton
    @Provides
    fun provideUserDao(db: FilterDatabase): CarFilterDao {
        return db.carFilterDao()
    }

    companion object {
        private const val BASE_URL = "http://api-aws-eu-qa-1.auto1-test.com/v1/"
    }
}
