package com.auto1.carfilter.di

import com.auto1.carfilter.MainActivity
import com.auto1.carfilter.ui.carmodels.CarModelsActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityModule {

    @ContributesAndroidInjector
    abstract fun contributeMainActivity(): MainActivity

    @ContributesAndroidInjector
    abstract fun contributeCarModelsActivity(): CarModelsActivity
}
