package com.auto1.carfilter.model

import com.google.gson.JsonElement
import com.google.gson.annotations.SerializedName
import org.json.JSONObject
import java.util.*

data class CarDataResponse(
    @SerializedName("page") var page: Int,
    @SerializedName("totalPageCount") var totalPage: Int,
    @SerializedName("wkda") var data: JsonElement
)