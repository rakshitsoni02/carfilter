package com.auto1.carfilter.model

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "car_manufacturers")
data class Manufacturer(
    @ColumnInfo(name = "manufacturerName")
    @NonNull
    var manufacturerName: String? = null,

    @NonNull
    @ColumnInfo(name = "manufacturerId")
    @PrimaryKey
    var manufacturerId: String = "",

    @ColumnInfo(name = "page")
    var page: Int? = null,

    @ColumnInfo(name = "totalPage")
    var totalPage: Int? = null
)