package com.auto1.carfilter.model

import androidx.annotation.NonNull
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity(tableName = "car_models")
data class Car(
    @NonNull
    var carName: String? = null,

    @NonNull
    @PrimaryKey
    var carId: String = "",

    var companyId: String? = null,

    var page: Int? = null,

    var totalPage: Int? = null,

    @Ignore
    var listBuiltDate: List<Car>? = null

) : Serializable