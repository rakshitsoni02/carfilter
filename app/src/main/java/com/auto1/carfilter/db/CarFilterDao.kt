package com.auto1.carfilter.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.auto1.carfilter.model.Car
import com.auto1.carfilter.model.Manufacturer


/**
 * Abstracts access to the filter database
 */
@Dao
interface CarFilterDao {

    /**
     * Insert Manufacturer into the database
     */
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertCarManufacturers(articles: List<Manufacturer>): List<Long>

    /**
     * Get all the Manufacturer from database
     */
    @Query("SELECT * FROM car_manufacturers  WHERE page = :page")
    fun getManufacturers(page: String): LiveData<List<Manufacturer>>

    @Query("SELECT totalPage,manufacturerId,MAX(totalPage) FROM car_manufacturers")
    fun loadByMaxCountManufacturer(): Manufacturer?


    /**
     * Insert Cars into the database
     */
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertCarModels(articles: List<Car>): List<Long>

    @Query("SELECT * FROM car_models  WHERE page = :page AND companyId=:companyId")
    fun getCars(page: String, companyId: String): LiveData<List<Car>>


    @Query("SELECT totalPage,carId,MAX(totalPage) FROM car_models WHERE companyId=:companyId")
    fun loadByMaxCountForCar(companyId: String): Car?

}