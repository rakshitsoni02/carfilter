package com.auto1.carfilter.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.auto1.carfilter.model.Car
import com.auto1.carfilter.model.Manufacturer


@Database(entities = [Manufacturer::class, Car::class], version = 1)
abstract class FilterDatabase : RoomDatabase() {

    /**
     * Get car Filter DAO
     */
    abstract fun carFilterDao(): CarFilterDao
}